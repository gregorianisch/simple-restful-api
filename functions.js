/*
 * Helper functions
 */

 // Return true in case string is a positive integer
 exports.isPositiveInteger = function(n) {
     return n >>> 0 === parseFloat(n);
 };

 /*
  Loop through organizations JSON and push items to array
  */
 exports.getOrganizationsTreeArray = function(organizations, children_key, result) {
     function processChildren(organization, res) {
         for (var i = 0; i < organization.length; i++) {
            var org_item = organization[i];
            result.push({org_item});
           if (children_key in org_item && org_item[children_key].length > 0) {
               processChildren(org_item[children_key], children_key, result);
           }
         }
     }
     processChildren(organizations, children_key, result);
 };
