/*
 * Main logic, creating server, routes and callbacks
 */

var restify     = require('restify') // building REST services
    ,env        = require('dotenv').config() // using environment variables
    ,async      = require('async') // working with asynchronous JavaScript
    ,_          = require('underscore') // different methods
    ,db         = require('./db') // connect database functions
    ,functions  = require('./functions'); // connect helper functions

// Create server
var server = restify.createServer({
  name: 'RESTful-API'
});

// Plugin is used to turn request data into a JavaScript object on the server automatically
server.use(restify.bodyParser());

// Plugin is used to parse the HTTP query string, is available in req.query
server.use(restify.queryParser());

// Define routes and callbacks
server.post('/organizations', postOrganizations); // curl -i -X POST -H 'Content-Type: application/json' -d  localhost:8080/organizations
server.get('/organizations', getOrganizations); // curl -i localhost:8080/organizations
server.put('/organizations', putOrganizations); // curl -i -X PUT http://localhost:8080/organizations

// Start server
server.listen(process.env.SERVER_PORT, function() {
  console.log('%s listening at %s', server.name, server.url);
});

/*
 * Add organizations
 * Function gets organizations tree and adds it to database
 */
function postOrganizations(req, res) {
  var organizations = []; // Create empty array for organizations
  functions.getOrganizationsTreeArray([req.body], 'daughters', organizations); // iterate through request json recursively and add to array

  // Start transaction
  db.startTransaction(req, function (err, result) {
    if (err) {
      res.send({'error':'startTransaction failed ' + err});
    }
  });

  //  Apply an async function to each array element in series, runs only a single async operation at a time.
  async.eachSeries(organizations, function(organization,  callback){
    var org_item = organization.org_item; // assign iterated array element to variable
    var org_id;  // create parent organization id variable
    var daughter_id;  // create daughter organization id variable

    // if array element has 'org_name'
    if (_.has(org_item, 'org_name')) {
      // if length of 'org_name' is more than 128 - throw error
      if (org_item.org_name.length > 128) {
         throw new Error('Organization with name ' + org_item.org_name
                         + ' is too long, 128 characters is a limit.');
      }
      // insert organization to db
      db.insertOrganization([org_item.org_name], function (err, result) {
        // return error in case it exists
        if (err) {
          return callback(err);
        }
        // assign result (inserted id) to parent id variable
        org_id = result[0]['result'];
        // if element has daughters
        if (_.has(org_item, 'daughters')) {
          // if daughters has element 'org_name'
          if (_.has(org_item.daughters, 'org_name')) {
            // insert daughter organization
            db.insertOrganization([org_item.daughters.org_name], function (err, result) {
              if (err) {
                return callback(err);
              }
              // assign id of created daughter organization to variable
              daughter_id = result[0]['result'];
              // insert daughter relationship with parent
              db.insertRelationship([org_id,daughter_id], function (err, result) {
                if (err) {
                  return callback(err);
                }
              });
            });
          } else {
            // irerate through daughters
            async.eachSeries(org_item.daughters, function(organization, callback) {
                // insert daughter organization
                db.insertOrganization([organization.org_name], function (err, result) {
                  if (err) {
                    return callback(err);
                  }
                  daughter_id = result[0]['result'];
                  // insert relationship
                  db.insertRelationship([org_id,daughter_id], function (err, result) {
                    if (err) {
                      return callback(err);
                    }
                    callback();
                  });
                });
            },
            // callback function for daughters loop
            function(err){
                if(err) {
                  db.rollback(req, function (err, result) {
                    if (err) {
                      return res.send({'error':'Rollback failed ' + err});
                    }
                  });
                  return res.send({'error': 'Adding daughters of organization failed. No organizations added.',
                                   'message': '' + err});
                }
                callback();
            });
          }
        } else {
          callback();
        }
      });
    }
  },
  // callback function for main loop
  function(err){
    if(err) {
      // rollback in case of error
      db.rollback(req, function (err, result) {
        if (err) {
          res.send({'error':'rollback failed ' + err});
        }
      });
      // send error in response
      res.send({'error': 'Adding organizations failed. No organizations added.',
                         'message': '' + err,
                         'object': '' + [org_item.org_name]});
    }
    // commit in case all fine
    db.commit(req, function (err, result) {
      if (err) {
        res.send({'error':'commit failed ' + err});
      }
    });
    // send message about succesful import
    res.send({'result': 'All data is inserted.'});
  });
}

/*
 * Get organization relations by name with offset
 * Returns relations ('parent','sister','daughters') of one organization with organization name
 * and offset set through parameter
 */
function getOrganizations(req, res) {

  // Set offset to 0 in case it's not provided
  if (req.params.offset == null ) {
    req.params.offset = 0;
  }

  // Validate input parameters
  if (!functions.isPositiveInteger(req.params.offset)) {
    res.send({'error':'Offset parameter should be number.'});
    return;
  } else if (req.params.name.length > 128) {
    res.send({'error':'Parameter name is too long, 128 is a limit.'});
    return;
  }

  db.getOrganizations([req.params.name,
                       req.params.offset], function (err, result) {
    if (err) {
      res.send({'error':'getOrganizations failed ' + err});
    } else if (_.isEmpty(result[0])) {
      res.send({'result':'There are no organization to show.'});
    } else {
      res.send({'organizations':result[0]});
    }
  });
}

/*
 * Mark all organizations as deleted, set 'is_deleted' to 1
 */
function putOrganizations(req,res) {

  // Set is_deleted to 0 in case it's not provided
  if (req.params.is_deleted == null ) {
    req.params.is_deleted = 1;
  }

  // Validate parameter is_deleted
  if (req.params.is_deleted != '0' && req.params.is_deleted != '1') {
    res.send({'error':'Parameter is_deleted should be 0 or 1.'});
    return;
  }

  db.putOrganizations([req.params.is_deleted],function (err, result) {
    if (err) {
      res.send({'error':'deleteAllOrganizations failed' + err});
    } else if (req.params.is_deleted == '1') {
      res.send({'result':'All organizations are deleted.'});
    } else if (req.params.is_deleted == '0') {
      res.send({'result':'All organizations are restored.'});
    }
  });
}
