/*
 * Connection to database, executing database queries
 */

var env   = require('dotenv').config() // using environment variables
    ,mysql = require('mysql'); // mysql module

// Connect to database
var connection = mysql.createConnection({
      host     : process.env.DB_HOST,
      user     : process.env.DB_USER,
      password : process.env.DB_PASS,
      database : process.env.DB_NAME
});

// Handle connection
connection.connect(function(err) {
  if (err) {
    console.log("DB conn error..." + err.stack);
  } else {
    console.log("DB connected :)");
  }
});

// Get organizations by name with offset
exports.getOrganizations = function (data, callback) {
  var queryParams = {
    name          : data[0],
    offset        : data[1]
  };
  var query = connection.query("CALL p_organization_get_relations_by_name(?,?);",
                               [ queryParams['name'],
                                 queryParams['offset']
                               ], callback);
  // console.log(query.sql);
};

// Insert organization name and return id of inserted row
exports.insertOrganization = function (data, callback) {
  var queryParams = {
    name          : data[0]
  };
  var query = connection.query("SELECT f_organization_create(?) result FROM DUAL;",
                                [queryParams['name']
                                ], callback);
};

// Insert organization relationship
exports.insertRelationship = function (data, callback) {
  var queryParams = {
    organization_id  : data[0],
    daughter_id      : data[1]
  };
  var query = connection.query("INSERT INTO organization_relationships VALUES(?,?,DEFAULT);",
                                [queryParams['organization_id'],
                                 queryParams['daughter_id']
                                ], callback);
};

// Update all organizations parameter 'is_deleted' to 0 (active) or 1 (inactive)
exports.putOrganizations = function (data, callback) {
  var queryParams = {
    is_deleted    : data[0]
  };
  console.log(queryParams);
  var query = connection.query("CALL p_organization_mark_all_as_deleted(?);",
                               [queryParams['is_deleted']
                               ], callback);
};

// Start transaction
exports.startTransaction = function (data, callback) {
  var query = connection.query("BEGIN;", callback);
};

// Commit all changes
exports.commit = function (data, callback) {
  var query = connection.query("COMMIT;", callback);
};

// Rollback all changes
exports.rollback = function (data, callback) {
  var query = connection.query("ROLLBACK;", callback);
};
