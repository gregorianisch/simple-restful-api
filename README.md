# REST-API service

## Keywords
Node.js, restify, mysql, dotenv, hierarchy, async.

## Description
RESTful-service for adding organizations tree, getting organizations hierarchy and deleting all data.

## Prerequisites
* Node.js (https://nodejs.org/en/)
* MySQL (https://www.mysql.com)
* Postman (optional) (https://www.getpostman.com)

## Install
* execute `npm install`
* import `sql/db.sql` and `sql/procedures.sql` into DB
* fill `.env` with your DB credentials if needed, set port on which you want to run an application
* execute `node index.js` to start service

## Service endpoints
* POST: `localhost:3000/organizations` - add organizations tree
* PUT: `localhost:3000/organizations` - delete or restore all organizations
* GET: `localhost:3000/organizations?name=Brown%20Banana&offset=100` - get organization relations

## Usage
* Postman collections for main usage and testing can be found in: `postman_collections`
* Add organizations, JSON examples: `json/add_ordanizations.json` or `json/add_ordanizations_2000.json`.
* Get organization relationships by name
* Mark all organizations as deleted
* Mark all organizations as restored

## Node modules used
* async (https://github.com/caolan/async)
* dotenv (https://github.com/bkeepers/dotenv)
* mysql (https://github.com/mysqljs/mysql)
* restify (http://restify.com)
* underscore (http://underscorejs.org)

## Performance
* Performance when adding organizations might be an issue. Adding organizations with relationships for ~2000 unique organizations has taken ~2 seconds (file `json/add_ordanizations_2000.json` was used).
It can be improved through reviewing used loops (perhaps loops number can be decreased) and better JSON parsing possibilities. Using stored procedure can give only small performance increase, so they are used only to keep code less overkilled with queries. Transaction, commit and rollback for large set of data should be done in iterations (commit each 100 000 row for example).

* Getting organization relationships is more important, as adding new is made usually only once or rarely.   Using DB cache (mysql-cache module for example) can increase SELECT performance. Also query itself might be more performative (decrease JOIN usage).

## Features
* Deleting data from database through API is not recommended, so it was decided that
  organizations can't be deleted from database, IS_DELETED column is used instead,
  this column will be set to value 1 in case organizations are marked as deleted.
* Assumption is made, that character limitation for organization name is set to 128 characters.
* Organizations marked as deleted can be restored with `localhost:3000/organizations?is_deleted=0`

## Improvement ideas
* Checking of organization name uniqueness before inserting it to database, currently it doesn't return
any error if added organization is not unique.
* Logging of events (https://github.com/trentm/node-bunyan for example) to understand what is going on.

## Example of adding organization
* POST: `localhost:3000/organizations`

```json
{  
   "org_name":"Paradise Island",
   "daughters":[  
      {  
         "org_name":"Banana tree",
         "daughters":[  
            {  
               "org_name":"Yellow Banana"
            },
            {  
               "org_name":"Brown Banana"
            },
            {  
               "org_name":"Black Banana"
            }
         ]
      },
      {  
         "org_name":"Big banana tree",
         "daughters":[  
            {  
               "org_name":"Yellow Banana"
            },
            {  
               "org_name":"Brown Banana"
            },
            {  
               "org_name":"Green Banana"
            },
            {  
               "org_name":"Black Banana",
               "daughters":[  
                  {  
                     "org_name":"Phoneutria Spider"
                  }
               ]
            }
         ]
      }
   ]
}
```

## Example of getting organization relationships ordered by name with offset

* GET: `localhost:3000/organizations?name=Brown%20Banana&offset=100`

```json
{  
   "organizations":[  
      {  
         "name":"Big banana tree",
         "role":"sister"
      },
      {  
         "name":"Black Banana",
         "role":"daughter"
      },
      {  
         "name":"Brown Banana",
         "role":"daughter"
      },
      {  
         "name":"Paradise Island",
         "role":"parent"
      },
      {  
         "name":"Yellow Banana",
         "role":"daughter"
      }
   ]
}
```
