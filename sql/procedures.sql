/*
 * Create procedures and functions
 */

-- Create function to create organization and return its id
DELIMITER $$

DROP FUNCTION IF EXISTS `f_organization_create`$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_organization_create`(
    `param_name`        VARCHAR(128)
    ) RETURNS int(11)
BEGIN
  DECLARE organization_id INT;

  SELECT CASE WHEN count(*) = 0 THEN 0 ELSE `id` END INTO `organization_id` FROM `organization`
        WHERE `name` = `param_name` AND `is_deleted` = 0;

  IF `organization_id` = 0 THEN
    INSERT INTO `organization` VALUES (DEFAULT,`param_name`,DEFAULT);
    SELECT LAST_INSERT_ID() INTO `organization_id` FROM DUAL;
  END IF;

  RETURN `organization_id`;

END$$

DELIMITER ;

-- Create function to get id of organization by name
DELIMITER $$

DROP FUNCTION IF EXISTS `f_get_organization_id_by_name`$$

CREATE DEFINER=`root`@`localhost` FUNCTION `f_get_organization_id_by_name`(
    `param_name` VARCHAR(128)) RETURNS int(11)
BEGIN
  DECLARE org_id INT;
  SELECT `id` INTO org_id FROM `organization`
    WHERE `name` = `param_name`
     AND `is_deleted` = 0;
  RETURN org_id;
END$$

DELIMITER ;

-- Create procedure to get relations of organization
DELIMITER $$

DROP PROCEDURE IF EXISTS `p_organization_get_relations_by_name`$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `p_organization_get_relations_by_name`(
    `param_name`   VARCHAR(128) CHARSET utf8,
    `param_offset` INT UNSIGNED)
    COMMENT 'Query all relations by a organization name, return as a result set'
BEGIN
  DECLARE
    org_id INT;
  SELECT
    f_get_organization_id_by_name(`param_name`) INTO  `org_id` FROM DUAL;

  SELECT * FROM (
   SELECT `name` as org_name, "parent" as relationship_type FROM `organization_relationships` rel
         LEFT JOIN `organization` org ON rel.`organization_id` = org.`id`
       WHERE rel.`daughter_id` =  `org_id` AND org.`is_deleted` = 0
       UNION
       SELECT `name` as org_name, "daughter" as relationship_type FROM `organization_relationships` rel
         LEFT JOIN `organization` org ON rel.`daughter_id` = org.`id`
       WHERE rel.`organization_id` =  `org_id` AND org.`is_deleted` = 0
       UNION
       SELECT DISTINCT `name` as org_name, "sister" as relationship_type FROM `organization_relationships` rel
         LEFT JOIN `organization` org ON rel.`daughter_id` = org.`id`
       WHERE rel.`organization_id` IN (SELECT rel.`organization_id` FROM `organization_relationships` rel
                                        WHERE rel.`daughter_id` = `org_id`)
          AND rel.daughter_id != `org_id`
          AND org.`is_deleted` = 0
   ) r
   ORDER BY r.`org_name`
      LIMIT 100 OFFSET `param_offset`;
END$$

DELIMITER ;

-- Create procedure to delete all organizations and their descendants
DELIMITER $$

DROP PROCEDURE IF EXISTS `p_organization_mark_all_as_deleted`$$

CREATE DEFINER = `root` @`localhost` PROCEDURE `p_organization_mark_all_as_deleted` (
    `param_is_deleted` INT UNSIGNED
) COMMENT 'Delete an organization and its descendants (update is_deleted = 1)'
BEGIN
  UPDATE
      `organization` AS org
      SET org.`is_deleted` = `param_is_deleted`;
  UPDATE
      `organization_relationships` AS rel
      SET rel.`is_deleted` = `param_is_deleted`;
END $$

DELIMITER ;
