/*
 * Create database structure: database, user, tables, constraints, grant permissions.
 */

-- Create database
CREATE DATABASE IF NOT EXISTS organizations;

USE organizations;

-- Create app user
CREATE USER IF NOT EXISTS
  'app'@'localhost' IDENTIFIED BY 'somepassword';

-- Create tables with keys
CREATE TABLE IF NOT EXISTS `organization` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `name` varchar(128) NOT NULL COMMENT 'Organization name',
  `is_deleted` tinyint(1) DEFAULT '0' NOT NULL COMMENT 'Is organizatin deleted (1) or not (0)',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Table for storing unique organizations id and names';

CREATE TABLE IF NOT EXISTS `organization_relationships` (
  `organization_id` int(11) NOT NULL COMMENT 'Organization id',
  `daughter_id` int(11) NOT NULL COMMENT 'Daughter id',
  `is_deleted` tinyint(1) DEFAULT '0' NOT NULL COMMENT 'Is organization deleted (1) or not (0)',
  PRIMARY KEY (`organization_id`,`daughter_id`),
  KEY `organization_id` (`organization_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT 'Table for storing relationships of organizations';

-- Add constraints
ALTER TABLE `organization`
  ADD CONSTRAINT `uk_organization` UNIQUE(`name`,`is_deleted`);

ALTER TABLE `organization_relationships`
  ADD CONSTRAINT `fk_organization_relationships_daughter_id` FOREIGN KEY (`daughter_id`)
    REFERENCES `organization` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_organization_relationships_organization_id` FOREIGN KEY (`organization_id`)
    REFERENCES `organization` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `uk_organization_relationships` UNIQUE(`daughter_id`,`organization_id`,`is_deleted`);

/*
 * Grant permissions to user. Delete permissions and access
 * to other tables are prohibited because of security matter
 */
GRANT INSERT, UPDATE, SELECT ON organizations.organization TO 'app'@'localhost';
GRANT INSERT, UPDATE, SELECT ON organizations.organization_relationships TO 'app'@'localhost';
GRANT EXECUTE ON PROCEDURE organizations.p_organization_get_relations_by_name TO 'app'@'localhost';
GRANT EXECUTE ON FUNCTION organizations.f_get_organization_id_by_name TO 'app'@'localhost';
GRANT EXECUTE ON PROCEDURE organizations.p_organization_mark_all_as_deleted TO 'app'@'localhost';
